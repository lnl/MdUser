<?php

Route::group(['middleware' => ['web','auth']], function () {

    Route::get('usuario/{id?}',  'LocknLoad\MdUser\Crud@get');
    Route::post('usuario/persist' , 'LocknLoad\MdUser\Crud@save');
    Route::delete('usuario/persist',  'LocknLoad\MdUser\Crud@delete');

});
