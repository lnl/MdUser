<?php

namespace LocknLoad\MdUser;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__. '/models/UsrLead.php';
        include __DIR__. '/models/UsrPerfil.php';
        include __DIR__. '/models/UsrUsuario.php';
        include __DIR__. '/models/UsrLog.php';

        $this->loadMigrationsFrom(__DIR__.'/migrations');
    //    $this->loadSeedsFrom(__DIR__.'/seeds');

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Routes.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {


     }
}
