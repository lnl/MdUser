<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class UsrPerfil extends ModelCore
{

    use SoftDeletes;

    protected $table = 'usr_perfis';
	protected $softDelete = true;
}
