<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;
use \Auth;
use \Request;

class UsrLog extends ModelCore
{
    use SoftDeletes;

    protected $table = 'usr_logs';
    protected $dates = ['deleted_at'];

    public static function generate($acao, $modelo, $conteudo = null){

        $log = new UsrLog();
        $log->id_usr_profile = Auth::user()->profile->id;
        $log->url = Request::url();
        $log->model = $modelo;
        $log->action = $acao;
        $log->value = $conteudo;
        $log->save();

    }

}
