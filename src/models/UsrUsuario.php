<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsrUsuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usr_usuarios';
    protected $softDelete = true;
    protected static $fieldsInList;
    protected static $orderBy;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','b_ativo', 'nivel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function __construct(){
        static::$orderBy       = "email";
        static::$fieldsInList  = array('email','ativo','nivel');
    }

    public function setPasswordAttribute($pass){
      $this->attributes['password'] = \Hash::make($pass);
    }

    public function level(){
        return $this->id_sc_nivel;
    }

    public function getColumns(){
        return \DB::select('SHOW COLUMNS FROM '.$this->table);
    }

    public static function getFieldsInList(){
        return isset(static::$fieldsInList) && !empty(static::$fieldsInList)? static::$fieldsInList : null;
    }

    public static function getOrderBy(){
        return isset(static::$orderBy)? static::$orderBy : null;
    }

    public static function getExtraActions(){
        return isset(static::$extraActions) && !empty(static::$extraActions)? static::$extraActions : null;
    }

    public static function getExcludeArr(){
      return isset(static::$excludeArr)? static::$excludeArr : null;
    }

    public function presentation(){
        return $this->email;
    }

    public function profile()
    {
        return $this->hasOne('App\UsrPerfil', 'id_usr_usuario', 'id');
    }

}
