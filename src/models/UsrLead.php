<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class UsrLead extends ModelCore
{
    use SoftDeletes;

    protected $table = 'usr_leads';
    protected $dates = ['deleted_at'];

}
