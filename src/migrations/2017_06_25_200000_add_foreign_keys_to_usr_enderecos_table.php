<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsrEnderecosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usr_enderecos', function(Blueprint $table)
		{
			$table->foreign('id_gb_cidade')->references('id')->on('gb_cidades')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usr_enderecos', function(Blueprint $table)
		{
			$table->dropForeign('usr_enderecos_id_gb_cidade_foreign');
		});
	}

}
