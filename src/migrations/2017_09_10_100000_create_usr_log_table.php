<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsrLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usr_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_usr_usuario')->unsigned()->index('usr_logs_id_usr_usuario_foreign');
			$table->string('url');
			$table->string('model');
			$table->string('acao', 100);
			$table->text('conteudo');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usr_logs');
	}

}
