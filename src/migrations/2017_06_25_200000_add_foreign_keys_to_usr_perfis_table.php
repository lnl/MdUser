<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsrPerfisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usr_perfis', function(Blueprint $table)
		{
			$table->foreign('id_gb_imagem', 'fk_usr_perfis_1')->references('id')->on('gb_imagens')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('id_cobranca_endereco')->references('id')->on('usr_enderecos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_entrega_endereco')->references('id')->on('usr_enderecos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_usr_usuario')->references('id')->on('usr_usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usr_perfis', function(Blueprint $table)
		{
			$table->dropForeign('fk_usr_perfis_1');
			$table->dropForeign('usr_perfis_id_cobranca_endereco_foreign');
			$table->dropForeign('usr_perfis_id_entrega_endereco_foreign');
			$table->dropForeign('usr_perfis_id_usr_usuario_foreign');
		});
	}

}
