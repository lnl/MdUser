<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsrUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usr_usuarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('senha', 100)->nullable();
			$table->string('remember_token', 200)->nullable();
			$table->enum('nivel', array('0','3','4','8','9'))->nullable();
			$table->boolean('b_admin');
			$table->boolean('b_ativo')->default(1);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usr_usuarios');
	}

}
