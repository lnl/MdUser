<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsrEnderecosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usr_enderecos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_gb_cidade')->unsigned()->index('usr_enderecos_id_gb_cidade_foreign');
			$table->string('rua');
			$table->string('numero', 5);
			$table->string('complemento', 100);
			$table->string('cep', 50);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usr_enderecos');
	}

}
