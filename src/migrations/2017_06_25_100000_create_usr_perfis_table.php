<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsrPerfisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usr_perfis', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_usr_usuario')->unsigned()->index('usr_perfis_id_usr_usuario_foreign');
			$table->integer('id_entrega_endereco')->unsigned()->nullable()->index('usr_perfis_id_entrega_endereco_foreign');
			$table->integer('id_cobranca_endereco')->unsigned()->nullable()->index('usr_perfis_id_cobranca_endereco_foreign');
			$table->integer('id_gb_imagem')->unsigned()->nullable()->index('fk_usr_perfis_1_idx');
			$table->string('nome', 100)->nullable();
			$table->string('sobrenome', 100)->nullable();
			$table->string('cpf', 45)->nullable();
			$table->string('rg', 45)->nullable();
			$table->enum('sexo', array('F','M'))->nullable();
			$table->dateTime('data_nasc')->nullable();
			$table->string('telefone', 45)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usr_perfis');
	}

}
