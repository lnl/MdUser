<?php
namespace LocknLoad\MdUser;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use LocknLoad\Crud\Helper;
use App\UsrUsuario;
use Illuminate\Support\Facades\Hash;

/**
 * Crud
 *
 * @uses BaseController
 * @package locknload\Mduser
 * @version //autogen//
 * @copyright Copyright (c) 2010 All rights reserved.
 * @author Davi Menegotto
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class Crud extends BaseController
{

    /* public listar($class, $filtro = null, $condicao = null) {{{ */
    /**
     * listar
     *
     * @access public
     * @return void
     */
    public function get(Request $r, $id=null)
    {
        $usuario = ($id)? UsrUsuario::find($id) : null;
        return Helper::generateView('crud.usuario', ['usuario' => $usuario]);
    }

    public function save(Request $r){
        $id = $r->input('id');
        $usuario = (!empty($id))? UsrUsuario::find($id) : new UsrUsuario();

        if(empty($id) && $id <= 0){
          $this->validate($r, [
             'email' => 'required|unique:usr_usuarios',
           ]);

          $usuario->email = $r->input('email');
        }

         if (!empty($r->input('password'))) {
           $this->validate($r, [
             'password' => 'required|min:3|confirmed',
           ]);

           $usuario->password =  $r->input('password');
         }

         $usuario->b_admin  = $r->input('b_admin') == 'on' ? 1:0;
         $usuario->b_ativo  = $r->input('b_ativo') == 'on' ? 1:0;
         $usuario->id_sc_nivel    = $r->input('nivel');

         $usuario->save();

         return Redirect::to('/listar/usr_usuario')->with('status','sucesso')->with('msg','Dados inseridos na base. ' );
    }

}
